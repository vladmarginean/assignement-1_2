package BusinessLayer;

import DataAccess.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by Vlad on 06.11.2016.
 */
@WebServlet(name = "LoginServlet", urlPatterns = {"/login"})
public class LoginServlet extends HttpServlet {
    private UserDAO userDao;

    public LoginServlet() {
        userDao = new UserDAO();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        if (username != null && username != "") {
            User dbUser = userDao.getUser(username);
            if (dbUser != null && password.equals(dbUser.getPassword())) {
                HttpSession session = request.getSession(false);
                if (session.getAttribute("username") == null) {
                    session.setAttribute("username", username);
                    session.setAttribute("access", dbUser.getAccess());
                }
                response.sendRedirect("/flights");
            } else {
                request.setAttribute("invalidCredentials", true);
                RequestDispatcher rd = request.getRequestDispatcher("/index.jsp");
                rd.forward(request, response);
            }
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        if (session != null) {
            session.invalidate();
        }
        response.sendRedirect("/");
    }
}
