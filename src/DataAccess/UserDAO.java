package DataAccess;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import javax.jws.soap.SOAPBinding;

/**
 * Created by Vlad on 30.10.2016.
 */
public class UserDAO {
    public User getUser(String username) {
        Session session = AirportSessionFactory.getSession();
        User user = null;
        try {
            Criteria criteria = session.createCriteria(User.class);
            user = (User) criteria.add(Restrictions.eq("username", username)).uniqueResult();
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
        return user;
    }
}
